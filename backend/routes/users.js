var secrets = require('../config/secrets');
const User = require('../models/user');
const Task = require('../models/task');

module.exports = function (router) {

    var users = router.route('/users');

    // Error handling helper function
    function sendErrorResponse(res, message, data, statusCode) {
        res.status(statusCode).send({message, data});
    }

    // Parsing helper functions
    function parseJSONParam(param, paramName, res) {
        if (param !== undefined && param !== null && param !== '') {
            try {
                return JSON.parse(param);
            } catch (err) {
                sendErrorResponse(res, `"${paramName}" data is not properly formatted JSON`, param, 400);
                return null;
            }
        }
        return {};
    }


    /* GET */
    function parseIntParam(param) {
        const number = parseInt(param, 10);
        return !isNaN(number) ? number : undefined;
    }

    users.get(function (req, res) {
        // Parse query parameters
        var where = parseJSONParam(req.query.where, 'where', res);
        var sort = parseJSONParam(req.query.sort, 'sort', res);
        var select = req.query.select ? parseJSONParam(req.query.select, 'select', res) : '_id name email pendingTasks dateCreated';
        var skip = parseIntParam(req.query.skip);
        var limit = parseIntParam(req.query.limit);
        var count = req.query.count === 'true';

        // Early return if parsing failed
        if (where === null || sort === null || select === null) return;

        // Build and execute the query
        var query = User.find(where);
        if (sort) query = query.sort(sort);
        if (skip !== undefined) query = query.skip(skip)
        if (limit !== undefined) query = query.limit(limit);
        if (typeof select === 'object') query = query.select(select);

        query.exec(function (err, users) {
            if (err) {
                sendErrorResponse(res, 'Error while processing "where" JSON query', where, 500);
            } else {
                var data = count ? users.length : users;
                res.status(200).send({message: 'OK', data});
            }
        });
    });


    /* POST User */
    users.post(async function (req, res) {
        var newUser = new User(req.body);
        newUser.dateCreated = new Date(req.body.dateCreated || Date.now());
        newUser.pendingTasks = req.body.pendingTasks || [];
        newUser.identify = req.body.identify ;

        if (!newUser.name || !newUser.email) {
            return res.status(400).send({
                message: 'Name and/or email was not specified',
                data: newUser
            });
        }

        try {
            const existingUser = await User.findOne({ email: newUser.email }).exec();
            if (existingUser) {
                return res.status(409).send({
                    message: 'User with specified email already exists',
                    data: existingUser
                });
            }

            // Update tasks and filter out the ones that don't exist
            newUser.pendingTasks = await Promise.all(
                newUser.pendingTasks.map(async (taskID) => {
                    const foundTask = await Task.findOneAndUpdate(
                        { _id: taskID },
                        { $set: { assignedUser: newUser._id, assignedUserName: newUser.name }},
                        { new: true }
                    ).exec();

                    return foundTask ? taskID : null;
                })
            );

            // Remove null values which represent tasks that were not found
            newUser.pendingTasks = newUser.pendingTasks.filter(taskID => taskID !== null);

            const savedUser = await newUser.save();
            res.status(201).send({
                message: 'Created',
                data: savedUser
            });
        } catch (err) {
            res.status(500).send({
                message: 'Server error while processing request',
                data: err.toString()
            });
        }
    });


    /* GET user:id */
    var userId = router.route('/users/:id');
    userId.get(async function (req, res) {
        const userID = req.params.id;

        try {
            const foundUser = await User.findById(userID).exec();

            if (!foundUser) {
                return res.status(404).send({
                    message: 'User with specified ID does not exist',
                    data: userID
                });
            }

            res.status(200).send({
                message: 'OK',
                data: foundUser
            });
        } catch (err) {
            res.status(500).send({
                message: 'Server error while trying to find user by ID',
                data: userID
            });
        }
    });


    /* PUT user:id */
    userId.put(async function (req, res) {
        const userID = req.params.id;
        const updateData = req.body;

        // Validate the necessary fields
        if (!updateData.name || !updateData.email) {
            return res.status(400).send({
                message: 'Name and/or email was not specified',
                data: updateData
            });
        }

        // Set dateCreated to current date if it's not valid or not provided
        updateData.dateCreated = new Date(updateData.dateCreated || Date.now());
        if (isNaN(updateData.dateCreated)) {
            updateData.dateCreated = new Date();
        }

        // change pendingTasks to array
        updateData.pendingTasks = updateData.pendingTasks || [];

        try {
            // Find the user and update their data
            const updatedUser = await User.findOneAndUpdate(
                { _id: userID },
                { $set: updateData },
                { new: true, runValidators: true }
            ).exec();

            if (!updatedUser) {
                return res.status(404).send({
                    message: 'User with specified ID does not exist',
                    data: userID
                });
            }

            // Update the associated tasks with the new user information
            const tasksUpdate = await Task.updateMany(
                { _id: { $in: updatedUser.pendingTasks } },
                { $set: { assignedUser: userID, assignedUserName: updatedUser.name } }
            ).exec();

            // Respond with the updated user data
            res.status(200).send({
                message: 'OK',
                data: updatedUser
            });
        } catch (err) {
            res.status(500).send({
                message: 'Server error while trying to update user',
                data: err.toString()
            });
        }
    });


    /* DELETE user:id */
    userId.delete(async function (req, res) {
        const userID = req.params.id;

        try {
            // Attempt to find and delete the user
            const foundUser = await User.findOneAndDelete({ _id: userID }).exec();

            if (!foundUser) {
                return res.status(404).send({
                    message: 'User with specified ID does not exist',
                    data: userID
                });
            }

            // Unlink original pendingTasks from the deleted user
            if (foundUser.pendingTasks && foundUser.pendingTasks.length > 0) {
                await Task.updateMany(
                    { _id: { $in: foundUser.pendingTasks } },
                    { $set: { assignedUser: "", assignedUserName: "unassigned" } }
                ).exec();
            }

            res.status(200).send({
                message: 'User successfully deleted',
                data: foundUser
            });
        } catch (err) {
            // General error handling
            res.status(500).send({
                message: 'Server error while attempting to delete user',
                data: err.toString()
            });
        }
    });

    return router;
}
