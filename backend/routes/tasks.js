var secrets = require('../config/secrets');
const User = require('../models/user');
const Task = require('../models/task');

module.exports = function (router) {

    var tasks = router.route('/tasks');

    /* GET */
    tasks.get(async function(req, res) {
        // Helper function to parse JSON or return undefined
        const parseJSON = (param) => {
            try {
                return JSON.parse(param);
            } catch (err) {
                return undefined; // Return undefined to indicate failure
            }
        };

        let { where, sort, select, skip, limit, count } = req.query;

        // Process query parameters
        where = parseJSON(where) || {};
        sort = parseJSON(sort) || {};
        select = parseJSON(select);
        skip = parseInt(skip, 10);
        limit = parseInt(limit, 10);
        count = count === 'true';

        // Ensure select is a string of fields separated by spaces
        if (select) {
            select = Object.entries(select).map(([key, value]) =>
                value === 1 ? key : `-${key}`
            ).join(' ');
        } else {
            select = '_id name description deadline completed assignedUser assignedUserName dateCreated';
        }

        try {
            // Use Mongoose query builder for chaining
            const query = Task.find(where).sort(sort).skip(skip).limit(limit).select(select);
            const tasks = await query.exec();

            const data = count ? tasks.length : tasks;
            res.status(200).send({ message: 'OK', data: data });
        } catch (err) {
            res.status(500).send({
                message: 'Server error while processing request',
                data: err.toString()
            });
        }
    });

    /* POST */
    tasks.post(async function(req, res) {
        try {
            let newTask = new Task({
                ...req.body,
                deadline: new Date(req.body.deadline),
                dateCreated: new Date(req.body.dateCreated || Date.now()),
                description: req.body.description || "",
                assignedUser: req.body.assignedUser || "",
                assignedUserName: req.body.assignedUserName || "unassigned",
                completed: req.body.completed === true,
                createdBy: req.body.createdBy || "" // [new]
            });

            // Validate 'name' and 'deadline'
            if (!newTask.name || isNaN(newTask.deadline)) {
                return res.status(400).send({
                    message: 'Name and/or valid deadline must be specified',
                    data: newTask
                });
            }

            // Save the new task
            await newTask.save();

            // If the task is not completed and assignedUser is set, link it to the user's pendingTasks
            // if (!newTask.completed && newTask.assignedUser) {
            if (newTask.assignedUser) {
                const foundUser = await User.findByIdAndUpdate(
                    newTask.assignedUser,
                    { $addToSet: { pendingTasks: newTask._id } },
                    { new: true }
                ).exec();

                // Check if assignedUserName matches foundUser's name
                if (foundUser && newTask.assignedUserName !== "unassigned" && newTask.assignedUserName !== foundUser.name) {
                    throw new Error("Task assignedUserName does not match assignedUser's name");
                }
                newTask.assignedUserName = foundUser ? foundUser.name : "unassigned";
            }

            res.status(201).send({
                message: 'Created',
                data: newTask
            });
        } catch (err) {
            res.status(500).send({
                message: 'Server error while trying to add new task to database',
                data: err.toString()
            });
        }
    });

    /* GET tasks:id */
    var tasksGet = router.route('/tasks/:id');
    tasksGet.get(async function(req, res) {
        const taskID = req.params.id;

        try {
            const foundTask = await Task.findById(taskID).exec();

            if (!foundTask) {
                return res.status(404).send({
                    message: 'Task with specified ID does not exist',
                    data: taskID
                });
            }

            res.status(200).send({
                message: 'OK',
                data: foundTask
            });
        } catch (err) {
            res.status(500).send({
                message: 'Server error while trying to find task by ID',
                data: err.toString()
            });
        }
    });

    /* PUT tasks:id */
    tasksGet.put(async function(req, res) {
        const taskID = req.params.id;
        const updateData = req.body;

        // Validate the provided data
        if (!updateData || typeof updateData !== 'object') {
            return res.status(400).send({
                message: 'Invalid task update data provided',
                data: {}
            });
        }

        // Convert and validate 'completed' field explicitly
        if ('completed' in updateData) {
            updateData.completed = updateData.completed === 'true' || updateData.completed === true;
        }

        // Parse and validate deadline
        if (updateData.deadline) {
            updateData.deadline = new Date(updateData.deadline);
            if (isNaN(updateData.deadline.getTime())) {
                return res.status(400).send({
                    message: 'Invalid deadline date format',
                    data: updateData.deadline
                });
            }
        }

        try {
            // Update the task
			const oldTask = await Task.findById(taskID).exec();
			if (!oldTask){
				return sendResponse(res, 404, "ERROR", "Invalid Task ID");
			}
			// if have old assigned user, update the old assigned user
			// if (oldTask.assignedUser && oldTask.assignedUser.toString() != updateData.assignedUser){
			// 	await User.findByIdAndUpdate(oldTask.assignedUser, {$pull: {pendingTasks: oldTask._id}})
			// }
            if ('assignedUser' in updateData && oldTask.assignedUser && oldTask.assignedUser.toString() != updateData.assignedUser){
                await User.findByIdAndUpdate(oldTask.assignedUser, {$pull: {pendingTasks: oldTask._id}});
            }

            const foundTask = await Task.findByIdAndUpdate(taskID, updateData, { new: true }).exec();

            if (!foundTask) {
                return res.status(404).send({
                    message: 'Task with specified ID does not exist',
                    data: taskID
                });
            }

			// not necessary to delete the taskID if mark completed.
			// if unexcepted error occurs, better to uncommanded the following code
            // // If the task is marked as completed, potentially update related user's pending tasks
            // if (updateData.completed) {
            //     await User.updateMany(
            //         { pendingTasks: taskID },
            //         { $pull: { pendingTasks: taskID } }
            //     ).exec();
            // } else 
			if (updateData.assignedUser) { // If not completed and an assignedUser is provided
                const assignedUser = await User.findById(updateData.assignedUser).exec();
                if (assignedUser) {
                    foundTask.assignedUserName = assignedUser.name;
                    await User.findByIdAndUpdate(
                        updateData.assignedUser,
                        { $addToSet: { pendingTasks: taskID } }
                    ).exec();
                } else {
                    // If assigned user is not found, set assignedUserName to "unassigned"
                    foundTask.assignedUserName = "unassigned";
                }
            }

            // Save the task if there were changes to the assigned user
            await foundTask.save();

            res.status(200).send({
                message: 'Task updated successfully',
                data: foundTask
            });
        } catch (err) {
            res.status(500).send({
                message: 'Server error while trying to update task',
                data: err.toString()
            });
        }
    });


    /* DELETE tasks:id */
    tasksGet.delete(async function(req, res) {
        const taskID = req.params.id;

        try {
            const foundTask = await Task.findById(taskID).exec();

            if (!foundTask) {
                return res.status(404).send({
                    message: 'Task with specified ID does not exist',
                    data: taskID
                });
            }

            // Remove the task from any user's pendingTasks before deleting it
            if (foundTask.assignedUser) {
                await User.findByIdAndUpdate(
                    foundTask.assignedUser,
                    { $pull: { pendingTasks: taskID } }
                ).exec();
            }

            // Now delete the task
            await Task.deleteOne({ _id: taskID }).exec();

            res.status(200).send({
                message: 'Task successfully deleted',
                data: foundTask
            });
        } catch (err) {
            res.status(500).send({
                message: 'Server error while trying to delete task',
                data: err.toString()
            });
        }
    });


    return router;
}
