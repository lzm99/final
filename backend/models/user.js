// Load required packages
var mongoose = require('mongoose');

// Define our user schema
var UserSchema = new mongoose.Schema({
    name:         String,
    email:        String,
    pendingTasks: [String], // task id list
    identify:     Boolean,   // 0:manager 1:staff [new]

    dateCreated:  Date
});

// Export the Mongoose model
module.exports = mongoose.model('User', UserSchema);
