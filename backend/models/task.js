// Load required packages
var mongoose = require('mongoose');

// Define our user schema
var TaskSchema = new mongoose.Schema({
    name:             String,
    deadline:         Date,
    description:      String,
    completed:        Boolean,
    assignedUser:     String,   // staff id -> staff name
    assignedUserName: String,   // staff name
    createdBy:        String,   // manger id [new]

    dateCreated:      Date
});

// Export the Mongoose model
module.exports = mongoose.model('Task', TaskSchema);