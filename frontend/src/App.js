import React from "react"
import { Container } from "react-bootstrap"
import { AuthProvider } from "./components/authentication/AuthContext"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import Dashboard from "./components/Dashboard"
import Signup from "./components/authentication/Signup"
import Login from "./components/authentication/Login"
import PrivateRoute from "./components/authentication/PrivateRoute"
import ForgotPassword from "./components/authentication/ForgotPassword"
import TaskDetail from "./components/TaskDetail"
import TaskCreation from './components/TaskCreation';


function App() {
  return (
      <Container>
        <div>
          <Router>
            <AuthProvider>
              <Switch>
                <PrivateRoute exact path="/" component={Dashboard}/>
                <Route path="/signup" component={Signup}/>
                <Route path="/login" component={Login}/>
                <Route path="/forgot-password" component={ForgotPassword}/>
                <PrivateRoute path="/task/:id" component={TaskDetail}/>
                <PrivateRoute path="/create-task" component={TaskCreation}/>
              </Switch>
            </AuthProvider>
          </Router>
        </div>
      </Container>
  )
}

export default App