import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import axios from 'axios';
import { useAuth } from './authentication/AuthContext';


// 用户界面，根据Manager和Staff显示不同内容。
export default function Dashboard() {

    // useAuth获取当前用户的状态。
    const { currentUser } = useAuth();

    // 获取角色。
    const [role, setRole] = useState("");

    // userDetails: 为 return 当中用的user object， 也是当前登陆进来的用户的个人信息。
    const [userDetails, setUserDetails] = useState(null);

    // tasks: Manager 可以查看到的所有task object 列表。
    const [tasks, setTasks] = useState([]);

    // uncompletedTasks: Staff completed=false的task object 列表。
    const [uncompletedTasks, setUncompletedTasks] = useState([]); // 员工的待完成任务

    // 方便返回上一级。
    const history = useHistory();

    // 根据 currentUser 的状态，获取以下数据。
    useEffect(() => {

        // 获取全部users的信息，
        axios.get("http://localhost:4000/api/users")
            .then(response => {
                const users = response.data.data;
                // 从所有users obj种看哪一位的email和currentUser的email相同，以此确定currentUser 的user obj 。
                const matchedUser = users.find(user => user.email === currentUser.email);

                // 如果成功找到，
                if (matchedUser) {

                    // 更新 userDetails 以备 return 使用。
                    setUserDetails(matchedUser);

                    // 设置角色。
                    setRole(matchedUser["identify"] ? "Manager" : "Staff");

                    console.log(matchedUser["identify"], typeof matchedUser["identify"]);
                    console.log("matchedUser: ");
                    console.log(matchedUser);
                    console.log("_id")
                    console.log(matchedUser["_id"]);

                    // ！如果是Staff身份！matchedUser["identify"]=false!
                    if (!matchedUser["identify"]) {
                        console.log("staff")

                        // 用 user obj id 获取 api/users/id 的个人信息内容。
                        axios.get(`http://localhost:4000/api/users/${matchedUser["_id"]}`)
                            .then(response => {

                                // 取出pendingTasks，which is a list of task obj id 。 task的compelted可能为true也可能false 要筛选。
                                const pendingTaskIds = response.data.data.pendingTasks;

                                // 使用 Promise.all 并行二次获取所有任务的详细信息！
                                const taskPromises = pendingTaskIds.map(taskId =>
                                    axios.get(`http://localhost:4000/api/tasks/${taskId}`)
                                );
                                Promise.all(taskPromises)
                                    .then(results => {
                                        const pendingTasks = results
                                            .map(result => result.data.data)
                                            // 只要 completed = false 的 task ！
                                            //TODO:NEED ALL
                                            // .filter(task => !task.completed);
                                            // Sort tasks with completed: false coming first
                                            .sort((a, b) => {
                                                // First, sort by completed (false first)
                                                if (a.completed !== b.completed) {
                                                    return a.completed ? 1 : -1;
                                                }
                                                
                                                // If completed values are the same, sort by deadline
                                                const deadlineA = new Date(a.deadline).getTime();
                                                const deadlineB = new Date(b.deadline).getTime();
                                
                                                return deadlineA - deadlineB;
                                            });
                                        // 存储到uncompletedTasks 中。
                                        setUncompletedTasks(pendingTasks);
                                    })
                                    .catch(error => {
                                        console.error("Error fetching task details", error);
                                    });
                            })
                            .catch(error => {
                                console.error("Error fetching user's pending tasks", error);
                            });
                    }

                    // ！如果为Manager身份！matchedUser["identify"]=true !
                    if (matchedUser["identify"]) {
                        console.log("manager")

                        // Manager 有权限获取数据库中所有tasks objects 。
                        axios.get("http://localhost:4000/api/tasks")
                            .then(response => {
                                const allTasks = response.data.data;
                        
                                // Sort tasks by completed (false first) and then by deadline
                                const sortedTasks = allTasks.sort((a, b) => {
                                    // First, sort by completed (false first)
                                    if (a.completed !== b.completed) {
                                        return a.completed ? 1 : -1;
                                    }
                        
                                    // If completed values are the same, sort by deadline
                                    const deadlineA = new Date(a.deadline).getTime();
                                    const deadlineB = new Date(b.deadline).getTime();
                        
                                    return deadlineA - deadlineB;
                                });
                        
                                // Set the sorted tasks in the state
                                setTasks(sortedTasks);
                            })
                            .catch(error => {
                                console.error("Error fetching tasks", error);
                            });
                    }
                }
            })
            .catch(error => {
                console.error("Error fetching users", error);
            });
    }, [currentUser]);

    console.log("uncompletedTasks");
    console.log(uncompletedTasks);

    console.log("tasks");
    console.log(tasks);

    // ！Staff 功能，可以选择 complete 某个任务。
    const handleTaskCompletion = (taskId) => {
        // ！Staff update task completed false -> true 是从 api/tasks/id 进行PUT的！
        // 点按钮，更新任务状态为true 。
        if (window.confirm("Are you sure you have completed this task?")) {
            axios.put(`http://localhost:4000/api/tasks/${taskId}`, {completed: true})
                .then(() => {
                    // ！ 重新更新需要展现出来的uncompletedTasks ！ 把当前点击已完成的去除掉！
                    //TODO:DO NOT DELETE
                    // setUncompletedTasks(prevTasks => prevTasks.filter(task => task._id !== taskId));
                    setUncompletedTasks(prevTasks => 
                        prevTasks.map(task => 
                            task._id === taskId ? { ...task, completed: true } : task
                        )
                    );
                    
                    
                })
                .catch(error => {
                    console.error("Error completing task", error);
                });
        }
    };

    // ！ Manager功能，可以进入 /create-task 的react 界面。
    const handleCreateTask = () => {
        history.push('/create-task');
    };

    return (
        <div>
            <h1> User Dashboard </h1>

            {/* userDetails 获取成功 */}
            {userDetails && (
                <div>
                    <p> Welcome, {role}: {userDetails.email} </p>

                    {/* ! 给 Staff 显示的内容是 uncompletedTasks ！a list of task info card . */}
                    { !userDetails["identify"] && uncompletedTasks.map(task => (
                        <Card key={task._id} className="mb-2">
                            <Card.Body>
                                <Card.Title>{task.name}</Card.Title>

                                {/* mm/dd/yyyy, hh:mm AM/PM */}
                                <Card.Text>
                                    Deadline: {
                                    task.deadline ? new Date(task.deadline).toLocaleString('en-US', {
                                        year: 'numeric',
                                        month: '2-digit',
                                        day: '2-digit',
                                        hour: '2-digit',
                                        minute: '2-digit',
                                        hour12: true // 使用 12 小时制
                                    }) : 'No Deadline'
                                }
                                    <br />

                                    Description: {task.description}
                                    <br />

                                    Completed: {task.completed ? "Yes" : "No"}
                                </Card.Text>

                                {/* ！可以点击的Finished 按钮！*/}
                                { !task.completed && (
                                    <button onClick={() => handleTaskCompletion(task._id)}> Finished </button>
                                )}

                            </Card.Body>
                        </Card>
                    ))}

                    {/* ! 给 Manager 显示的内容是数据库中所有tasks。 */}
                    {userDetails["identify"] && tasks.map(task => (
                        <Card key={task._id} className="mb-2">
                            <Card.Body>

                                <Card.Title>{task.name}</Card.Title>

                                {/* mm/dd/yyyy, hh:mm AM/PM */}
                                <Card.Text>
                                    Deadline: {
                                    task.deadline ? new Date(task.deadline).toLocaleString('en-US', {
                                        year: 'numeric',
                                        month: '2-digit',
                                        day: '2-digit',
                                        hour: '2-digit',
                                        minute: '2-digit',
                                        hour12: true
                                    }) : 'No Deadline'
                                }
                                    <br />

                                    Description: {task.description}
                                    <br />

                                    Completed: {task.completed ? "Yes" : "No"}
                                    <br />

                                    Assigned Staff ID: {task.assignedUser}
                                    <br />

                                    Assigned Staff Account: {task.assignedUserName}
                                </Card.Text>

                                {/* ！只有Manager可以进入任务细节界面，以便更改。任务列表上的每个item都该可以进入 react 任务细节界面，这里路径的id 用 task obj id！*/}
                                <Link to={`/task/${task._id}`} className="btn btn-primary">
                                    View Task Details
                                </Link>

                            </Card.Body>
                        </Card>
                    ))}

                    {/* ！只有 Manager 才能有 task creation 功能！*/}
                    { userDetails["identify"] && (
                        <button onClick={handleCreateTask}> Create Task </button>
                    )}
                </div>
            )}

            {/* 返回 login 。*/}
            <Link to="/login" className="btn btn-primary w-100 mt-3">Log Out</Link>
        </div>
    );
}

