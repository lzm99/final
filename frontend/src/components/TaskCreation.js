import React, { useState, useEffect, useCallback } from 'react';
import { useHistory, Link } from 'react-router-dom';
import axios from 'axios';
import './Task.css';


// ! Manger 才可以通过按钮进入 tesk creation 界面。
function TaskCreation() {

    const [formData, setFormData] = useState({
        name: '',
        deadline: '',
        description: '',
        // 固定设置成false。
        completed: 'false',
        assignedUser: '',
        assignedUserEmail: '',
        assignedUserName: '',
        // 固定设置成当前日期。
        createdBy: new Date().toISOString().slice(0, 10)
    });
    const [users, setUsers] = useState([]);
    const history = useHistory();
    const [tooltip, setTooltip] = useState({ show: false, message: '', x: 0, y: 0 });


    // 获取所有user 给 下拉菜单准备。
    // useEffect(() => {
    //     axios.get("http://localhost:4000/api/users")
    //         .then(response => {
    //             setUsers(response.data.data);
    //         })
    //         .catch(error => {
    //             console.error("Error fetching users", error);
    //         });
    // }, []);
    useEffect(() => {
        axios.get("http://localhost:4000/api/users")
            .then(response => {
                // Filter users where identify is equal to 0
                const filteredUsers = response.data.data.filter(user => user.identify === false);
    
                // Set the filtered users in the state
                setUsers(filteredUsers);
            })
            .catch(error => {
                console.error("Error fetching users", error);
            });
    }, []);
    


    // 表单改变。
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData(prevFormData => ({
            ...prevFormData,
            [name]: value
            // [name]: name === 'deadline' ? value : value.trim(),
        }));
    };


    // 下拉菜单选择email改变。
    const handleUserEmailChange = (e) => {
        const selectedUser = users.find(user => user.email === e.target.value);
        setFormData(prevFormData => ({
            ...prevFormData,
            assignedUser: selectedUser._id,
            assignedUserEmail: selectedUser.email,
            assignedUserName: selectedUser.name
        }));
    };

    // Submit 。
    const handleSubmit = (e) => {
        e.preventDefault();

        e.preventDefault();

        // // 解析 deadline 字符串为 Date 对象。
        // const deadlineParts = formData.deadline.split(", ");
        // const datePart = deadlineParts[0];
        // const timePart = deadlineParts[1];
        // const [month, day, year] = datePart.split("/");
        // const [hours, minutes] = timePart.split(":");
        // const deadlineDate = new Date(year, month - 1, day, hours, minutes);
        const deadlineDate = new Date(formData.deadline);

        const submitData = {
            ...formData,
            deadline: deadlineDate,
            completed: formData.completed === 'true'
        };

        if (window.confirm("Are you sure you want to submit this task?")) {
            axios.post("http://localhost:4000/api/tasks", submitData)
                .then(response => {
                    console.log(response.data);
                    history.push("/");
                })
                .catch(error => {
                    console.error("Error creating task", error);
                });
        }
    };


    // Cancel ，点击以后相当于啥也没做。
    const handleCancel = () => {
        if (window.confirm("Are you sure you want to cancel?")) {
            // 清空表单数据！
            setFormData({
                name: '',
                deadline: '',
                description: '',
                completed: 'false',
                assignedUser: '',
                assignedUserEmail: '',
                assignedUserName: '',
                createdBy: new Date().toISOString().slice(0, 10)
            });

            // 重定向回上一级。
            history.goBack();
        }
    };


    // 以下functions 是对于用户点击了readOnly 的input 的时候展现的 message 。
    const handleReadOnlyClick = (e, message) => {
        console.log("click on read only input ");
        // message 位置和show=true。
        setTooltip({
            show: true,
            message: message,
            x: e.clientX,
            y: e.clientY + 20
        });
    };
    // 点击read only input 以外的范围可以让message消失。
    const handleOutsideClick = useCallback((e) => {
        console.log("click outside read only input")
        if (tooltip.show) {
            console.log("make tooltip disappear")
            setTooltip({ ...tooltip, show: false });
        }
        // 依赖项是 tooltip。
    }, [tooltip]);
    // 根据tooltip message出现效果。监听。
    useEffect(() => {
        console.log("tooltip effect")
        window.addEventListener('click', handleOutsideClick);
        return () => {
            console.log("outside")
            window.removeEventListener('click', handleOutsideClick);
        };
        // useEffect 的依赖项现在包括 handleOutsideClick。
    }, [handleOutsideClick]);


    return (
        <div>
            <h1> Create Task </h1>

            <form onSubmit={handleSubmit}>

                <div>
                    <label> Task Name: </label>
                    <input
                        type="text"
                        name="name"
                        value={formData.name}
                        onChange={handleChange}
                    />
                </div>

                <div>
                    <label> Deadline (mm/dd/yyyy, hh:mm military time): </label>
                    <input
                        // type="text"
                        type="datetime-local"
                        name="deadline"
                        placeholder="MM/DD/YYYY, HH:MM"
                        value={formData.deadline}
                        // value={formData.deadline}
                        onChange={handleChange}
                    />
                </div>

                <div>
                    <label> Description: </label>
                    <textarea
                        name="description"
                        value={formData.description}
                        onChange={handleChange}
                    />
                </div>

                {/* Completed Read Only 。只能是 false 。 */}
                <div>
                    <label>Completed:</label>
                    <input
                        type="text"
                        name="completed"
                        value={formData.completed}
                        readOnly
                        onClick={(e) => handleReadOnlyClick(e, "Read only, cannot be edited." +
                            " When a task is created, the completed status must be false.")}
                    />
                </div>

                {/* 下拉菜单选择分配用户 */}
                <div>
                    <label> Assign to (Choose Staff Account): </label>
                    <select
                        name="assignedUserEmail"
                        value={formData.assignedUserEmail}
                        onChange={handleUserEmailChange}
                    >
                        <option value="">Select a User</option>
                        {users.map(user => (
                            <option key={user._id} value={user.email}>
                                {user.email}
                            </option>
                        ))}
                    </select>
                </div>

                {/* Assigned User ID 根据 上一步选择的email 自动更新。 Read Only 。*/}
                <div>
                    <label> Assigned User ID: </label>
                    <input
                        type="text"
                        name="assignedUser"
                        value={formData.assignedUser}
                        readOnly
                        onClick={(e) => handleReadOnlyClick(e, "Read only, cannot be edited. " +
                            " When Manager wants to assign this task to a Staff, s/he should click the 'Assign to' drop down menu, and choose a Staff account to assign."
                           )}
                    />
                </div>

                {/* Date Created */}
                <div>
                    <label> Created By: </label>
                    <input
                        type="text"
                        name="createdBy"
                        value={formData.createdBy}
                        readOnly
                        onClick={(e) => handleReadOnlyClick(e, "Read only, cannot be edited. " +
                            " This datetime will be automatically updated after Manager submit this new task.")}
                    />
                </div>

                {/* tooltip */}
                {tooltip.show && (
                    <div
                        style={{
                            position: 'absolute',
                            top: tooltip.y,
                            left: tooltip.x,
                            backgroundColor: '#ffffe0', // 浅黄色背景
                            border: '1px solid #ccc', // 灰色边框
                            padding: '8px',
                            borderRadius: '5px',
                            boxShadow: '0 2px 4px rgba(0, 0, 0, 0.1)', // 阴影
                            zIndex: 1000,
                            fontSize: '14px',
                            color: '#333',
                            // 允许内容换行
                            whiteSpace: 'pre-wrap'
                        }}
                    >
                        {tooltip.message}
                    </div>
                )}


                <div className="form-buttons">
                    <button type="submit"> Submit </button>
                    <button type="button" onClick={handleCancel}> Cancel </button>
                </div>

                <Link to={`/`} className="btn btn-primary">
                    Return to Dashboard
                </Link>

            </form>
        </div>
    );
}

export default TaskCreation;

