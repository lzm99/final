import React, { useRef, useState } from "react"
import { Form, Button, Card, Alert } from "react-bootstrap"
import { Link, useHistory} from "react-router-dom"
import axios from 'axios';
import { useAuth } from "./AuthContext"


export default function Signup() {
  const emailRef = useRef()
  const passwordRef = useRef()
  const passwordConfirmRef = useRef()
  const { signup } = useAuth()
  const [error, setError] = useState("")
  const [loading, setLoading] = useState(false)
  const [role, setRole] = useState("");
  const history = useHistory()

  async function handleSubmit(e) {
    e.preventDefault()

    if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      return setError("Passwords do not match")
    }

    // 必须选择Manager或者Staff的身份。
    if (!role) {
      return setError("Please select a role")
    }

    try {
      setError("")
      setLoading(true)

      // Firebase 注册。
      await signup(emailRef.current.value, passwordRef.current.value, role);

      console.log("role:");
      console.log(role);

      // 创建新user object 。
      const newUser = {
        // user name 和 user email 一样。
        name: emailRef.current.value,
        email: emailRef.current.value,
        // identify 有问题，不管怎么传进去都是true，要改。我测试的时候Manager=1，Staff = 0了。
        identify: role === "Manager" ? true: false
      };

      console.log("newUser:")
      console.log(newUser)

      // 用axios.post to ../api/users。Firebase sign up 的时候同时 create new user object 。
      axios.post("http://localhost:4000/api/users", newUser)

      // 成功注册之后，返回上一界面，which is login 界面。
      alert("Registration Successful! Please log in.")
      history.push("/login")


    } catch {
      console.error("Error in handleSubmit: ", error);
      setError("An error occurred during sign up");
    }

    setLoading(false)
  }

  return (
      <>
        <Card>
          <Card.Body>
            <h2 className="text-center mb-4">Sign Up</h2>
            {error && <Alert variant="danger">{error}</Alert>}
            <Form onSubmit={handleSubmit}>
              <Form.Group id="email">
                <Form.Label>Email</Form.Label>
                <Form.Control type="email" ref={emailRef} required />
              </Form.Group>
              <Form.Group id="password">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" ref={passwordRef} required />
              </Form.Group>
              <Form.Group id="password-confirm">
                <Form.Label>Password Confirmation</Form.Label>
                <Form.Control type="password" ref={passwordConfirmRef} required />
              </Form.Group>
              {/* 增加选择identify/role的radio buttons 。 */}
              <Form.Group>
                <Form.Label>Role</Form.Label>
                <Form.Check
                    type="radio"
                    label="Manager"
                    name="role"
                    value="Manager"
                    onChange={(e) => setRole(e.target.value)}
                    checked={role === "Manager"}
                />
                <Form.Check
                    type="radio"
                    label="Staff"
                    name="role"
                    value="Staff"
                    onChange={(e) => setRole(e.target.value)}
                    checked={role === "Staff"}
                />
              </Form.Group>
              <Button disabled={loading} className="w-100" type="submit">
                Sign Up
              </Button>
            </Form>
          </Card.Body>
        </Card>
        <div className="w-100 text-center mt-2">
          Already have an account? <Link to="/login">Log In</Link>
        </div>
      </>
  )
}

