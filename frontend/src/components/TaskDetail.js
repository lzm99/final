import React, { useEffect, useState, useCallback } from 'react';
import { useParams, Link } from 'react-router-dom';
import axios from 'axios';


// 任务细节界面，只给 Manger 看！以便他们更新。Staff dashboard的task info card是没有这个选项的。
function TaskDetail() {

    // 抓取当前 url 的 id 。
    const { id } = useParams();
    
    // task : 当前的task obj 。
    const [task, setTask] = useState(null);

    // ！users : 所有 user obj 。reassign 任务的时候，可以分配给所有 users 任务。
    const [users, setUsers] = useState([]);

    // formData ： task 表单内容。也是要被 PUT 的内容。
    const [formData, setFormData] = useState({
        name: '',
        deadline: '',
        description: '',
        // 默认值，后续会被真实值填充。
        completed: false,
        assignedUser: '',
        assignedUserName: '',
        createdBy: ''
    });

    // ！用于保存最开始/被form修改前的task obj 的信息。
    const [originalTaskData, setOriginalTaskData] = useState({});
    
    // ！点击 Edit 按钮才可以修改，设置 mode ！
    const [editMode, setEditMode] = useState(false);

    // ！tooltip 提示框设定，给readonly 项准备！一开始show=false 。
    const [tooltip, setTooltip] = useState({ show: false, message: '', x: 0, y: 0 });



    // ! 将后端返回的日期转换为 datetime-local 格式
    const convertDateToDateTimeLocal = (date) => {
        if (!date) return '';
        const localDate = new Date(date);
        localDate.setMinutes(localDate.getMinutes() - localDate.getTimezoneOffset());
        return localDate.toISOString().slice(0, 16);
    };



    // 用 url 里 task id 的变化来捕获effect 。
    useEffect(() => {
        
        // ！用url 里的 task id 来抓取 api/tasks/id 的细节数据。
        axios.get(`http://localhost:4000/api/tasks/${id}`)
            .then(response => {

                const taskData = response.data.data;
                // set up task。
                setTask(taskData);

                // 表单初始化展现和被设定内容被设定成抓取的 task obj 的内容。
                setFormData({
                    ...taskData,
                    // ！转换日期格式 方便后续edit 。
                    deadline: convertDateToDateTimeLocal(taskData.deadline)
                });

                // set up originalTaskData。
                setOriginalTaskData({ 
                    ...taskData ,
                    deadline: convertDateToDateTimeLocal(taskData.deadline)
                });

            })
            .catch(error => {
                console.error("Error fetching task details", error);
            });

        // 获取所有user obj 。
        axios.get("http://localhost:4000/api/users")
            .then(response => {
                // Filter users where identify is equal to 0
                const filteredUsers = response.data.data.filter(user => user.identify === false);
    
                // Set the filtered users in the state
                setUsers(filteredUsers);
            })
            .catch(error => {
                console.error("Error fetching users", error);
            });
    }, [id]);


    // ！return 中 input 里的 name 和 value 属性会被监听到，name 会显示 value 的内容。
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData(prevFormData => ({
            // 保留其他字段不变。
            ...prevFormData,
            // 渲染 name 。
            [name]: name === "completed" ? (value === "True") : value
        }));
    };


    // ！Manager reassign 的时候，提供所有user email的下拉菜单。
    // ！如果某一个email被选中，assignedUser (ID) 和 assignedUserName (其实也是email) 会被自动更新成和选中email对应的用户信息。
    const handleUserEmailChange = (e) => {
        const selectedEmail = e.target.value;
        // 从 all user obj 对应 email 。
        const reassignedUser = users.find(user => user.email === selectedEmail);
        // 重新设置表单。
        setFormData(prevFormData => ({
            // 其他不变。
            ...prevFormData,
            // 改变assignedUser ID 和 Name=Email 的显示。
            assignedUser: reassignedUser ? reassignedUser._id : '',
            assignedUserName: reassignedUser ? reassignedUser.name : ''
        }));
    };



    // 点击 'Edit' 时的弹窗。
    const handleEditConfirmation = () => {
        if (window.confirm("Are you sure you want to edit this task?")) {
            setEditMode(true);
        }
    };


    // 点击Save ， put 表单内容到 api/tasks/id 以实现更新任务。
    const handleSave = (e) => {
        e.preventDefault();

        const updatedData = {
            ...formData,
            // ！将 datetime-local 转换为 Date 对象
            deadline: new Date(formData.deadline)
        };

        axios.put(`http://localhost:4000/api/tasks/${id}`, updatedData)
            .then(response => {
                // 重新设定当前展现的 task 。
                setTask(response.data.data);
                // ！点击save 以后 edit mode 为 false 了。
                setEditMode(false);
            })
            .catch(error => {
                console.error("Error updating task", error);
            });
    };

    // 点击 'Save' 时的弹窗。
    const handleSaveConfirmation = (e) => {
        if (window.confirm("Are you sure you want to save these changes?")) {
            handleSave(e);
        }
    };


    // 修改到一半不想修改的话就点击Cancel ， 直接取消所有更改，展现originalTaskData 。
    const handleCancel = () => {
        // ！重置表单数据为初始数据。
        setFormData(originalTaskData);
        // 退出编辑模式。
        setEditMode(false);
    };


    // Cancel 弹窗。
    const handleCancelConfirmation = () => {
        if (window.confirm("Are you sure you want to cancel your changes?")) {
            handleCancel();
        }
    };


    // 以下functions 是对于用户点击了readOnly 的input 的时候展现的 message 。
    const handleReadOnlyClick = (e, message) => {
        console.log("click on read only input ");
        // message 位置和show=true。
        setTooltip({
            show: true,
            message: message,
            x: e.clientX,
            y: e.clientY + 20
        });
    };
    // 点击read only input 以外的范围可以让message消失。
    const handleOutsideClick = useCallback((e) => {
        console.log("click outside read only input")
        if (tooltip.show) {
            console.log("make tooltip disappear")
            setTooltip({ ...tooltip, show: false });
        }
        // 依赖项是 tooltip。
    }, [tooltip]);
    // 根据tooltip message出现效果。监听。
    useEffect(() => {
        console.log("tooltip effect")
        window.addEventListener('click', handleOutsideClick);
        return () => {
            console.log("outside")
            window.removeEventListener('click', handleOutsideClick);
        };
        // useEffect 的依赖项现在包括 handleOutsideClick。
    }, [handleOutsideClick]);


    return (
        <div>
            <h1> Task Details </h1>

            <form onSubmit={handleSaveConfirmation}>

                {/* Task completed, created/updated by, assigned user ID, assigned user account 都是read only 。*/}

                <div>
                    <label> Task Name: </label>
                    <input
                        type="text"
                        name="name"
                        value={formData.name}
                        onChange={handleChange}
                        disabled={!editMode}
                    />
                </div>

                <div>
                    <label> Deadline (mm/dd-yyyy, hh:mm AM(PM)): </label>
                    <input
                        type="datetime-local"
                        name="deadline"
                        value={formData.deadline}
                        onChange={handleChange}
                        disabled={!editMode}
                    />
                </div>

                <div>
                    <label> Description: </label>
                    <textarea
                        name="description"
                        value={formData.description}
                        onChange={handleChange}
                        disabled={!editMode}
                    />
                </div>

                {editMode && (
                    <div>
                        <label> Completed/Incomplete: </label>
                        <select
                            name="completed"
                            value={formData.completed ? "True" : "False"}
                            onChange={handleChange}
                        >
                            <option value="True">Completed</option>
                            <option value="False">Incomplete</option>
                        </select>
                    </div>
                )}
                {!editMode && (
                    <div>
                        <label> Completed/Incomplete:  </label>
                        <span>
                                {formData.completed ? "Completed" : "Incomplete"}
                        </span>
                    </div>
                )}

                <div>
                    <label> Updated By: </label>
                    <input
                        type="text"
                        className="readonly-input"
                        name="dateCreated"
                        value={task ? new Date(task.dateCreated).toLocaleDateString() : ''}
                        readOnly
                        onClick={(e) => handleReadOnlyClick(e, "Read only, cannot be edited. " +
                            " This datetime will be automatically updated after Manager save the change.")}
                    />
                </div>

                <div>
                    <label>Assigned Staff ID:</label>
                    <input
                        type="text"
                        className="readonly-input"
                        name="assignedUser"
                        value={formData.assignedUser}
                        readOnly
                        onClick={(e) => handleReadOnlyClick(e, "Read only, cannot be edited. " +
                            " If the Manager wants to reassign this task to another Staff, s/he should click 'Edit' button, and a reassign account option will be shown up." +
                            " Manager can choose a new Staff's acount s/he wants to reassign this task to, after that, 'Assigned User ID' will be automatically updated to the new Staff.")}
                    />
                </div>

                <div>
                    <label>Assigned Staff Account:</label>
                    <input
                        type="text"
                        className="readonly-input"
                        name="assignedUserName"
                        value={formData.assignedUserName}
                        readOnly
                        onClick={(e) => handleReadOnlyClick(e, "Read only, cannot be edited." +
                            " If Manager wants to reassign this task to a new Staff, s/he should click 'Edit' button, and a reassign account option will be shown up. " +
                            " Manager can choose a new Staff's acount s/he wants to reassign this task to, after that, 'Assigned User ID' will be automatically updated to the new Staff.")}
                    />
                </div>

                {/* tooltip */}
                {tooltip.show && (
                    <div
                        style={{
                            position: 'absolute',
                            top: tooltip.y,
                            left: tooltip.x,
                            backgroundColor: '#ffffe0', // 浅黄色背景
                            border: '1px solid #ccc', // 灰色边框
                            padding: '8px',
                            borderRadius: '5px',
                            boxShadow: '0 2px 4px rgba(0, 0, 0, 0.1)', // 阴影
                            zIndex: 1000,
                            fontSize: '14px',
                            color: '#333',
                            // 允许内容换行
                            whiteSpace: 'pre-wrap'
                        }}
                    >
                        {tooltip.message}
                    </div>
                )}

                {/* 点击 Edit 之后进入 edit Mode 。*/}
                { editMode && (
                    <div>
                        <label> Choose the Staff you want to reassign this task to :</label> <br/>
                        <label> Staff account: </label>

                        {/* drop down menu 显示全部user email 。*/}
                        <select
                            name="assignedUserEmail"
                            onChange={handleUserEmailChange}
                            value={users.find(user => user._id === formData.assignedUser)?.email || ''}
                        >
                            {users.map(user => (
                                <option key={user._id} value={user.email}>{user.email}</option>
                            ))}
                        </select>
                    </div>
                )}

                {/* Edit button ， 可以控制editMode 。*/}
                <button type="button" onClick={handleEditConfirmation}> Edit </button>

                {/* Cancel button ， 可以控制editMode 。*/}
                <button type="button" onClick={handleCancelConfirmation} disabled={!editMode}> Cancel </button>

                {/* Save button ， 可以控制editMode 。*/}
                <button type="submit" disabled={!editMode}>Save</button>

                {/* 返回 Dashboard 。*/}
                <Link to={`/`} className="btn btn-primary">
                    Return to Dashboard
                </Link>

            </form>
        </div>
    );
}

export default TaskDetail;

