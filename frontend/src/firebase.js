import firebase from 'firebase/app'
import 'firebase/auth'

const app = firebase.initializeApp({
  apiKey: "AIzaSyAa3A_dISAGUzQ7M7b68KOfNqxstmnyy-I",
  authDomain: "cs409-a3c7c.firebaseapp.com",
  projectId: "cs409-a3c7c",
  storageBucket: "cs409-a3c7c.appspot.com",
  messagingSenderId: "702113267129",
  appId: "1:702113267129:web:630de9d767542fed1b8a68",
  measurementId: "G-42S0HJ1946"
});

export const auth = app.auth()
export default app